import os
import random
from collections import deque, namedtuple
import numpy as np
from tqdm import tqdm
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.autograd import Variable

from agents import TD3Agent, HeuristicAgent
from environments import DispatcherEnv
from utils import Transition, breakdown_state
from defaults import SEED, N_EPISODES_TEST, WARMUP_EPISODES, EPISODE_LEN
from defaults import REWARD_AGG_INTERVAL, N_RIDERS, DIST_MAT, ORDER_RATES
from defaults import N_AREAS, RIDER_CAPACITY, TRAIN_INTERVAL, EXPLORATION_NOISE
from defaults import MIN_ACTIONS, MAX_ACTIONS

USE_CUDA = torch.cuda.is_available()
FloatTensor = torch.cuda.FloatTensor if USE_CUDA else torch.FloatTensor
LongTensor = torch.cuda.LongTensor if USE_CUDA else torch.LongTensor
ByteTensor = torch.cuda.ByteTensor if USE_CUDA else torch.ByteTensor
DEVICE = torch.device("cuda" if USE_CUDA else "cpu")

MIN_ACTIONS_TENSOR = FloatTensor(MIN_ACTIONS)
MAX_ACTIONS_TENSOR = FloatTensor(MAX_ACTIONS)

def main():
    random.seed(SEED)
    np.random.seed(SEED)
    torch.manual_seed(SEED)

    env = DispatcherEnv(
        DIST_MAT, 
        N_RIDERS,
        betas = ORDER_RATES,
        episode_length = EPISODE_LEN,
        rider_capacity = RIDER_CAPACITY,
    )

    td3_agent = TD3Agent(
        env.state_dim,
        env.action_dim,
        policy_noise = FloatTensor([0.5 for i in range(N_RIDERS*N_AREAS)] + [0.5 for i in range(N_RIDERS)]),
        noise_clip = FloatTensor([1 for i in range(N_RIDERS*N_AREAS)] + [1 for i in range(N_RIDERS)]),
        min_actions = MIN_ACTIONS_TENSOR,
        max_actions = MAX_ACTIONS_TENSOR,
    )
    td3_agent.load(os.path.join("trained_agents", "dispatcher_td3", '_final'))

    ga_agent = HeuristicAgent(
        env.state_dim,
        env.action_dim,
        n_riders = N_RIDERS,
        dist_mat = DIST_MAT,
        rider_capacity = RIDER_CAPACITY,
    )
    ga_agent.load(os.path.join("trained_agents", "ga_agent"))

    timeliness_agent = HeuristicAgent(
        env.state_dim,
        env.action_dim,
        n_riders = N_RIDERS,
        dist_mat = DIST_MAT,
        rider_capacity = RIDER_CAPACITY,
        travel_time_buffer = [10,10,10],
        max_destinations = 1,
    )

    efficiency_agent = HeuristicAgent(
        env.state_dim,
        env.action_dim,
        n_riders = N_RIDERS,
        dist_mat = DIST_MAT,
        rider_capacity = RIDER_CAPACITY,
        travel_time_buffer = [0,0,0],
        max_destinations = 3,
    )

    to_test = [
        td3_agent,
        timeliness_agent,
        efficiency_agent,
        ga_agent,
    ]
    agent_names = [
        'td3_agent',
        'timeliness_agent',
        'efficiency_agent',
        'ga_agent',
    ]

    df_output = pd.DataFrame(
        columns = [
            "agent_name",
            "episode_num",
            "total_reward",
            "lateness_penalties",
            "earliness_rewards",
            "travel_penalties",
            "ontime_deliveries",
            "late_deliveries",
            "1_delivery_trips",
            "2_delivery_trips",
            "3_delivery_trips",
            "4_up_delivery_trips",
        ]
    )

    # TODO: display summary table
    df_summary = pd.DataFrame(columns = [
        "agent_name",
        "mean_reward",
        "median_reward",
        "mean_lateness_penalty",
        "mean_earliness_reward",
        "mean_travel_penalty",
        "mean_ontime_deliveries",
        "mean_late_deliveries",
        "mean_1_delivery_trips",
        "mean_2_delivery_trips",
        "mean_3_delivery_trips",
        "mean_4_up_delivery_trips",
    ])

    for i, agent_ in enumerate(to_test):
        print("Testing agent {}.".format(agent_names[i]))
        for episode in tqdm(range(N_EPISODES_TEST), ascii = True, unit = "episode"):

            episode_reward = 0
            lateness_penalty = 0
            earliness_reward = 0
            travel_penalty = 0
            ontime_deliveries = 0
            late_deliveries = 0
            n_delivery_trips = [0 for i in range(4)]
                # NOTE: index 3 is 4 or more deliveries

            step = 0
            if isinstance(agent_, TD3Agent):
                current_state = FloatTensor(env.reset())
            else:
                current_state = env.reset()
            is_done = False

            while not is_done:
                if isinstance(agent_, TD3Agent):
                    action = agent_.get_action(current_state).numpy()
                    action = np.round_(action).astype('int32')
                else:
                    action = agent_.get_action(breakdown_state(env))

                next_state, reward, is_done, info = env.step(action)

                episode_reward += reward
                lateness_penalty += info["lateness_penalty"]
                earliness_reward += info["early_reward"]
                travel_penalty += info["travel_penalty"]
                ontime_deliveries += sum(info["ontime_deliveries"])
                late_deliveries += sum(info["late_deliveries"])

                for rider_idx, n_deliveries in enumerate(info["dispatchment_deliveries"]):
                    if n_deliveries <= 0:
                        continue
                    if n_deliveries >= 4:
                        n_delivery_trips[3] += n_deliveries
                    else:
                        n_delivery_trips[n_deliveries - 1] += n_deliveries

                if isinstance(agent_, TD3Agent):
                    current_state = FloatTensor(next_state)
                else:
                    current_state = next_state
                step += 1
            
            df_output = df_output.append(
                {
                    "agent_name": agent_names[i],
                    "episode_num": episode,
                    "total_reward": episode_reward,
                    "lateness_penalties": lateness_penalty,
                    "earliness_rewards": earliness_reward,
                    "travel_penalties": travel_penalty,
                    "ontime_deliveries": ontime_deliveries,
                    "late_deliveries": late_deliveries,
                    "1_delivery_trips": n_delivery_trips[0],
                    "2_delivery_trips": n_delivery_trips[1],
                    "3_delivery_trips": n_delivery_trips[2],
                    "4_up_delivery_trips": n_delivery_trips[3],
                },
                ignore_index = True,
            )

        df_summary = df_summary.append(
            {
                "agent_name": agent_names[i],
                "mean_reward": np.mean(df_output.loc[df_output["agent_name"] == agent_names[i],"total_reward"]),
                "median_reward": np.median(df_output.loc[df_output["agent_name"] == agent_names[i],"total_reward"]),
                "mean_lateness_penalty": np.mean(df_output.loc[df_output["agent_name"] == agent_names[i],"lateness_penalties"]),
                "mean_earliness_reward": np.mean(df_output.loc[df_output["agent_name"] == agent_names[i],"earliness_rewards"]),
                "mean_travel_penalty": np.mean(df_output.loc[df_output["agent_name"] == agent_names[i],"travel_penalties"]),
                "mean_ontime_deliveries": np.mean(df_output.loc[df_output["agent_name"] == agent_names[i],"ontime_deliveries"]),
                "mean_late_deliveries": np.mean(df_output.loc[df_output["agent_name"] == agent_names[i],"late_deliveries"]),
                "mean_1_delivery_trips": np.mean(df_output.loc[df_output["agent_name"] == agent_names[i],"1_delivery_trips"]),
                "mean_2_delivery_trips": np.mean(df_output.loc[df_output["agent_name"] == agent_names[i],"2_delivery_trips"]),
                "mean_3_delivery_trips": np.mean(df_output.loc[df_output["agent_name"] == agent_names[i],"3_delivery_trips"]),
                "mean_4_up_delivery_trips": np.mean(df_output.loc[df_output["agent_name"] == agent_names[i],"4_up_delivery_trips"]),
            },
            ignore_index = True,
        )
    df_output["total_cost"] = -df_output["total_reward"]
    print(df_summary)

    # TODO: boxplot of rewards/costs
    fig, ax = plt.subplots()
    ax = sns.boxplot(x = "agent_name", y = "total_cost", data=df_output)
    ax.set_title("Agent Episode Costs")
    ax.set_ylabel("Episode Cost")
    ax.set_xlabel("Agent")

    # plt.show()
    plt.savefig(os.path.join("plots", "agent_costs.png"))
    plt.clf()

    # TODO: pie or stacked bar of ontime and late deliveries
    totals = [df_summary.loc[df_summary["agent_name"] == agname,"mean_ontime_deliveries"].sum() + df_summary.loc[df_summary["agent_name"] == agname,"mean_late_deliveries"].sum() for agname in agent_names]
    ontime_bars = [df_summary.loc[df_summary["agent_name"] == agname,"mean_ontime_deliveries"].sum()/totals[i] for i, agname in enumerate(agent_names)]
    late_bars = [df_summary.loc[df_summary["agent_name"] == agname,"mean_late_deliveries"].sum()/totals[i] for i, agname in enumerate(agent_names)]

    fig, ax = plt.subplots()
    ax.bar([i for i in range(len(agent_names))], ontime_bars, color = 'blue', width = 0.85, label = "Ontime Deliveries")
    ax.bar([i for i in range(len(agent_names))], late_bars, bottom = ontime_bars, color = 'orange', width = 0.85, label = "Late Deliveries")
    plt.xticks([i for i in range(len(agent_names))], agent_names)

    vals = ax.get_yticks()
    ax.set_yticklabels(['{:,.2%}'.format(x) for x in vals])
    ax.legend(loc='upper left')

    plt.savefig(os.path.join("plots", "agent_ontime_deliveries.png"))
    plt.clf()

    # TODO: stacked bar of 1, 2, 3 and 4+ delivery trips
    totals = [df_summary.loc[df_summary["agent_name"] == agname,"mean_1_delivery_trips"].sum()\
        + df_summary.loc[df_summary["agent_name"] == agname,"mean_2_delivery_trips"].sum()\
        + df_summary.loc[df_summary["agent_name"] == agname,"mean_3_delivery_trips"].sum()\
        + df_summary.loc[df_summary["agent_name"] == agname,"mean_4_up_delivery_trips"].sum() for agname in agent_names]
    one_del_bars = [df_summary.loc[df_summary["agent_name"] == agname,"mean_1_delivery_trips"].sum()/totals[i] for i, agname in enumerate(agent_names)]
    two_del_bars = [df_summary.loc[df_summary["agent_name"] == agname,"mean_2_delivery_trips"].sum()/totals[i] for i, agname in enumerate(agent_names)]
    three_del_bars = [df_summary.loc[df_summary["agent_name"] == agname,"mean_3_delivery_trips"].sum()/totals[i] for i, agname in enumerate(agent_names)]
    four_up_del_bars = [df_summary.loc[df_summary["agent_name"] == agname,"mean_4_up_delivery_trips"].sum()/totals[i] for i, agname in enumerate(agent_names)]
    print(four_up_del_bars)

    fig, ax = plt.subplots()
    ax.bar([i for i in range(len(agent_names))], one_del_bars, color = 'blue', width = 0.85, label = "1 Delivery Trips")
    ax.bar([i for i in range(len(agent_names))], two_del_bars, bottom = one_del_bars, color = 'orange', width = 0.85, label = "2 Delivery Trips")
    ax.bar([i for i in range(len(agent_names))], three_del_bars, bottom = np.add(one_del_bars,two_del_bars), color = 'green', width = 0.85, label = "3 Delivery Trips")
    ax.bar([i for i in range(len(agent_names))], four_up_del_bars, bottom = np.add(np.add(one_del_bars,two_del_bars),three_del_bars), color = 'red', width = 0.85, label = "4+ Delivery Trips")
    plt.xticks([i for i in range(len(agent_names))], agent_names)

    vals = ax.get_yticks()
    ax.set_yticklabels(['{:,.2%}'.format(x) for x in vals])
    ax.legend(loc='upper left')

    plt.savefig(os.path.join("plots", "agent_deliveries_per_trip.png"))
    plt.clf()

if __name__ == "__main__":
    main()