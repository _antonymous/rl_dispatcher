import os
import random
from collections import deque, namedtuple
import numpy as np
from copy import copy
import json

from utils import breakdown_state

def _fix_array(array_):
    fixed_array = []
    for item_ in array_:
        if isinstance(item_, np.ndarray):
            fixed_array.append(item_.item())
        else:
            fixed_array.append(item_)
    return fixed_array

class HeuristicAgent:
    '''
    Follows rules dictated by a vector provided by the Genetic Algorithm.
    Rules include:
        travel_time_buffer:
            The minimum buffer over the expected travel time to the rider's
                ealiest assigned delivery.
            Rider is automatically dispatched if the due time of the earliest asssigned
                delivery to an area is <= the current time + expected travel time + buffer
            If this is negative, then the agent will tend to give more allowance.
            Higher value for this means a rider will wait more for more deliveries,
                risking being late.
            Lower value prioritizes being early.
        max_destinations:
            The maximum number of different destinations a rider can have in a trip.
            If a rider already has this many destinations, the agent will not assign a delivery
                to a new destination.
            The agent will wait for another rider to be available if none are available.
    
    NOTE: this agent is aware only of the state vector, so it has equal
        knowledge as the RL agents.
    NOTE: it's possible to create an agent which is aware of the entire environment.
        so that it would have full knowledge.

    '''

    def __init__(self, state_dim, action_dim, **kwargs):
        self.state_dim = state_dim
        self.action_dim = action_dim

        self.n_riders = kwargs["n_riders"]
        self.dist_mat = kwargs["dist_mat"]
        self.rider_capacity = kwargs["rider_capacity"]

        self.n_areas = self.dist_mat.shape[0] - 1

        self.travel_time_buffer = kwargs.get("travel_time_buffer", [0 for i in range(self.n_areas)])
        self.max_destinations = kwargs.get("max_destinations", self.n_areas)
        
    def save(self, path_):

        if not os.path.exists(path_):
            os.makedirs(path_)

        with open(os.path.join(path_, "params.json"), "w") as f:
            json.dump(
                {
                    "state_dim": self.state_dim,
                    "action_dim": self.action_dim,
                    "n_riders": self.n_riders,
                    "rider_capacity": self.rider_capacity,
                    "n_areas": self.n_areas,
                    "travel_time_buffer": list(_fix_array(self.travel_time_buffer)),
                    "max_destinations": self.max_destinations,
                },
                f
            )

    def load(self, path_):

        with open(os.path.join(path_, "params.json"), "r") as f:
            params_ = json.load(f)

        self.state_dim = params_["state_dim"]
        self.action_dim = params_["action_dim"]
        self.n_riders = params_["n_riders"]
        self.rider_capacity = params_["rider_capacity"]
        self.n_areas = params_["n_areas"]
        self.travel_time_buffer = params_["travel_time_buffer"]
        self.max_destinations = params_["max_destinations"]

    def get_action(self, state_breakdown):
        # TODO: check for assignments to make
        # NOTE: action here is DDPG style
        # NOTE: priority 1 are riders who already have a delivery in the same area
        # NOTE: priority 2 are riders with no assigned deliveries yet
        # NOTE: priority 3 are riders with deliveries in other branches

        # NOTE: just invert areas and riders
        n_assigned_reverse = [[0 for i in range(self.n_areas)] for j in range(self.n_riders)]
        n_unassigned = copy(state_breakdown["n_unassigned"])
        assignments = [[0 for j in range(self.n_riders)] for i in range(self.n_areas)]

        for area_idx in range(1, self.n_areas + 1):
            for rider_idx in range(self.n_riders):
                n_assigned_reverse[rider_idx][area_idx - 1] +=\
                    state_breakdown["n_assigned"][(area_idx - 1)*self.n_areas + rider_idx]

        area_idx_shuffle = [i + 1 for i in range(self.n_areas)]
        np.random.shuffle(area_idx_shuffle)
            
        for area_idx in area_idx_shuffle:
            
            # NOTE: remember which priority were checking now
            priority_level = 1

            # if any deliveries to assign
            while n_unassigned[area_idx - 1] > 0:

                # check if any rider has only deliveries to this area and has more capacity
                if priority_level == 1:
                    priority_level = 2
                    for rider_idx in range(self.n_riders):
                        if (sum(n_assigned_reverse[rider_idx]) < self.rider_capacity) and (n_assigned_reverse[rider_idx][area_idx - 1] == sum(n_assigned_reverse[rider_idx])):
                            # NOTE: assign one to this rider
                            n_assigned_reverse[rider_idx][area_idx - 1] += 1
                            n_unassigned[area_idx - 1] -= 1
                            assignments[area_idx - 1][rider_idx] += 1
                            priority_level = 1

                # elif, check if any rider has no assigned deliveries
                elif priority_level == 2:
                    priority_level = 3
                    for rider_idx in range(self.n_riders):
                        if sum(n_assigned_reverse[rider_idx]) == 0:
                            n_assigned_reverse[rider_idx][area_idx - 1] += 1
                            n_unassigned[area_idx - 1] -= 1
                            assignments[area_idx - 1][rider_idx] += 1
                            priority_level = 2

                # elif, check if any rider has more capacity and would not exceed self.max_destinations
                elif priority_level == 3:
                    priority_level = 4
                    for rider_idx in range(self.n_riders):
                        n_dest = 0
                        for area_idx_ in range(1, self.n_areas + 1):
                            if n_assigned_reverse[rider_idx][area_idx_ - 1] > 0:
                                n_dest += 1

                        if (sum(n_assigned_reverse[rider_idx]) < self.rider_capacity) and (n_dest < self.max_destinations):
                            n_assigned_reverse[rider_idx][area_idx - 1] += 1
                            n_unassigned[area_idx - 1] -= 1
                            assignments[area_idx - 1][rider_idx] += 1
                            priority_level = 3

                # else, dont assign and break the while loop
                else:
                    break

        # TODO: check for dispatchments to make
        
        dispatchments = [0 for i in range(self.n_riders)]
        # for each rider
        for rider_idx in range(self.n_riders):
            # if rider in branch
            if state_breakdown["rider_in_branch"][rider_idx]:
                
                # if rider has any deliveries assigned
                if sum(n_assigned_reverse[rider_idx]) > 0:
                    # for each area
                    for area_idx in range(1, self.n_areas + 1):

                        # if earliest_undispatched for area (and rider?) is less than current time + expected travel time + buffer
                        if state_breakdown["earliest_undispatched"][(area_idx - 1)*self.n_areas + rider_idx] <=\
                            state_breakdown["time"] + self.dist_mat[0, area_idx] + self.travel_time_buffer[area_idx - 1]:

                            # NOTE: the error is that sometimes self.travel_time_buffer[area_idx - 1] is not a single value
                            # it means that it is not supplied correctly

                            # dispatch rider
                            dispatchments[rider_idx] = 1
                            # break area loop
                            break

                        # else, wait

        action_vector = []
        for area_assignments in assignments:
            action_vector += copy(area_assignments)
        action_vector += copy(dispatchments)

        return action_vector