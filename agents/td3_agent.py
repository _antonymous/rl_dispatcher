import os
import random
from collections import deque, namedtuple
import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.autograd import Variable

from utils import Transition

USE_CUDA = torch.cuda.is_available()
FloatTensor = torch.cuda.FloatTensor if USE_CUDA else torch.FloatTensor
LongTensor = torch.cuda.LongTensor if USE_CUDA else torch.LongTensor
ByteTensor = torch.cuda.ByteTensor if USE_CUDA else torch.ByteTensor
DEVICE = torch.device("cuda" if USE_CUDA else "cpu")

class ActorNetwork(nn.Module):

    _hidden1_dim = 256
    _hidden2_dim = 64

    def __init__(self, state_dim, action_dim, max_actions = None):
        super(ActorNetwork, self).__init__()

        self.max_actions = max_actions

        self.fc1 = nn.Linear(state_dim, self._hidden1_dim)
        self.fc3 = nn.Linear(self._hidden1_dim, action_dim)
        self.relu = nn.ReLU()
        self.tanh = nn.Tanh()

    def forward(self, state):
        action = self.relu(self.fc1(state))
        action = torch.mul(self.tanh(self.fc3(action)), self.max_actions)
        return action

class DoubleCriticNetwork(nn.Module):

    _hidden1_dim = 256
    _hidden2_dim = 64

    def __init__(self, state_dim, action_dim):
        super(DoubleCriticNetwork, self).__init__()

        # NOTE: critic 1
        self.fc1 = nn.Linear(state_dim + action_dim, self._hidden1_dim)
        self.fc3 = nn.Linear(self._hidden1_dim,1)
        
        # NOTE: critic 2
        self.fc4 = nn.Linear(state_dim + action_dim, self._hidden1_dim)
        self.fc6 = nn.Linear(self._hidden1_dim, 1)

        self.relu = nn.ReLU()

    def forward(self, state, action):
        state_action = torch.cat([state, action], 1)

        q1 = self.relu(self.fc1(state_action))
        q1 = self.fc3(q1)

        q2 = self.relu(self.fc4(state_action))
        q2 = self.fc6(q2)

        return q1, q2

    def get_q1(self, state, action):
        state_action = torch.cat([state, action], 1)

        q1 = self.relu(self.fc1(state_action))
        q1 = self.fc3(q1)

        return q1

class TD3Agent:

    def __init__(self, state_dim, action_dim, **kwargs):
        self.state_dim = state_dim
        self.action_dim = action_dim

        self.minibatch_size = kwargs.get("minibatch_size", 64)
        self.replay_memory_len = kwargs.get("replay_memory_len", 10_000)
        self.actor_lr = kwargs.get("actor_lr", 0.0001)
        self.critic_lr = kwargs.get("critic_lr", 0.001)
        self.tau = kwargs.get("tau", 0.001)
        self.discount = kwargs.get("discount", 0.99)
        self.min_actions = kwargs.get("min_actions", None)
        self.max_actions = kwargs.get("max_actions", None)

        # NOTE: replace these
        self.policy_noise = kwargs.get("policy_noise", FloatTensor([1 for i in range(action_dim)]))
        self.noise_clip = kwargs.get("noise_clip", FloatTensor([1 for i in range(action_dim)]))
        self.policy_update_interval = kwargs.get("policy_update_interval", 2)

        self.actor_network = ActorNetwork(
            self.state_dim,
            self.action_dim,
            max_actions = self.max_actions,
        ).to(DEVICE)
        self.actor_target = ActorNetwork(
            self.state_dim,
            self.action_dim,
            max_actions = self.max_actions,
        ).to(DEVICE)
        self.actor_optimizer = optim.Adam(
            self.actor_network.parameters(),
            lr = self.actor_lr,
        )

        self.critic_network = DoubleCriticNetwork(
            self.state_dim,
            self.action_dim,
        ).to(DEVICE)
        self.critic_target = DoubleCriticNetwork(
            self.state_dim,
            self.action_dim,
        ).to(DEVICE)
        self.critic_optimizer = optim.Adam(
            self.critic_network.parameters(),
            lr = self.critic_lr,
        )

        # NOTE: hard update target weights
        self._hard_update()

        self.replay_memory = deque(maxlen = self.replay_memory_len)

        self.reset()

    def reset(self):
        self.n_iterations = 0

    def _hard_update(self):
        self.actor_target.load_state_dict(self.actor_network.state_dict())
        self.actor_target.eval()

        self.critic_target.load_state_dict(self.critic_network.state_dict())
        self.critic_target.eval()

    def _soft_update(self):
        for target_param, source_param in zip(self.actor_target.parameters(), self.actor_network.parameters()):
            target_param.data.copy_(
                target_param*(1 - self.tau) + source_param*self.tau
            )

        for target_param, source_param in zip(self.critic_target.parameters(), self.critic_network.parameters()):
            target_param.data.copy_(
                target_param*(1 - self.tau) + source_param*self.tau
            )

    def update_replay_memory(self, transition):
        self.replay_memory.append(transition)

    def train(self):
        if len(self.replay_memory) < self.minibatch_size:
            return

        # NOTE: sample a minibatch
        transition_sample = random.sample(self.replay_memory, self.minibatch_size)
        minibatch = Transition(*zip(*transition_sample))

        # NOTE: prepare tensorts from sample
        current_states = torch.stack(minibatch.current_state)
        actions = torch.stack(minibatch.action)
            
        # NOTE: this time action is a vector
        next_states = torch.stack(minibatch.next_state)
        rewards = torch.cat(minibatch.reward)
        done_ = torch.cat(minibatch.is_done)

        noise = torch.mul(torch.randn_like(actions), self.policy_noise)
        noise = torch.max(torch.min(noise, self.noise_clip), -self.noise_clip)

        if USE_CUDA:
            current_states.cuda()
            actions.cuda()
            next_states.cuda()
            rewards.cuda()
            done_.cuda()
            noise.cuda()

        # NOTE: get actions of the target network to check how good they are
        next_actions = self.actor_target(next_states) + noise
        next_actions = torch.max(torch.min(next_actions, self.max_actions), self.min_actions)

        target_q1, target_q2 = self.critic_target(next_states, next_actions)

        # NOTE: should be max since the problem is cost minimization?
        next_qs = torch.min(target_q1, target_q2)

        target_qs = rewards + (1 - done_)*self.discount*next_qs

        current_q1, current_q2 = self.critic_network(
            current_states,
            actions,
        )
        critic_loss = F.smooth_l1_loss(
            current_q1,
            target_qs
        ) + F.smooth_l1_loss(
            current_q2,
            target_qs
        )
        self.critic_optimizer.zero_grad()
        critic_loss.backward()
        self.critic_optimizer.step()

        if self.n_iterations % self.policy_update_interval == 0:
            actor_loss = self.critic_network.get_q1(
                current_states,
                self.actor_network(current_states)
            ).mean()

            self.actor_optimizer.zero_grad()
            actor_loss.backward()
            self.actor_optimizer.step()

            self._soft_update()

        self.n_iterations += 1

    def get_action(self, state):
        if USE_CUDA:
            return self.actor_network(state).detach().cpu()
        else:
            return self.actor_network(state).detach()

    def save(self, path_):
        # NOTE: create directory if it does not exist
        if not os.path.exists(path_):
            os.makedirs(path_)

        # NOTE: save only the live networks then just use self._hard_update() on load
        torch.save(self.actor_network.state_dict(), os.path.join(path_, "actor_network.pt"))
        torch.save(self.critic_network.state_dict(), os.path.join(path_, "critic_network.pt"))

    def load(self, path_):
        if USE_CUDA:
            map_location = "cuda"
        else:
            map_location = "cpu"

        self.actor_network = ActorNetwork(
            self.state_dim,
            self.action_dim,
            max_actions = self.max_actions,
        )

        self.actor_network.load_state_dict(
            torch.load(
                os.path.join(path_, "actor_network.pt"),
                map_location = map_location,
            )
        )

        self.critic_network = DoubleCriticNetwork(
            self.state_dim,
            self.action_dim,
        )

        self.critic_network.load_state_dict(
            torch.load(
                os.path.join(path_, "critic_network.pt"),
                map_location = map_location,
            ),
        )

        self._hard_update()