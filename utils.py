import queue
from collections import deque, namedtuple
import numpy as np

Transition = namedtuple('Transition', (
    'current_state',
    'action',
    'next_state',
    'reward',
    'is_done'
))

def breakdown_state(env):
    state = env.get_state()
    breakdown = {}
    breakdown["n_processing"] = state[:env.n_areas]
    breakdown["n_unassigned"] = state[env.n_areas:2*env.n_areas]
    breakdown["rider_in_branch"] = state[2*env.n_areas:2*env.n_areas + env.n_riders]
    breakdown["n_assigned"] = state[2*env.n_areas + env.n_riders:2*env.n_areas + env.n_riders + env.n_areas*env.n_riders]
    breakdown["rider_destinations"] = state[2*env.n_areas + env.n_riders + env.n_areas*env.n_riders:2*env.n_areas + 2*env.n_riders + env.n_areas*env.n_riders]
    breakdown["earliest_unassigned"] = state[2*env.n_areas + 2*env.n_riders + env.n_areas*env.n_riders:3*env.n_areas + 2*env.n_riders + env.n_areas*env.n_riders]
    breakdown["earliest_undispatched"] = state[3*env.n_areas + 2*env.n_riders + env.n_areas*env.n_riders : 3*env.n_areas + 2*env.n_riders + 2*env.n_areas*env.n_riders]
    breakdown["last_dispatch_time"] = state[3*env.n_areas + 2*env.n_riders + 2*env.n_areas*env.n_riders : 3*env.n_areas + 3*env.n_riders + 2*env.n_areas*env.n_riders]
    breakdown["n_late_processing"] = state[3*env.n_areas + 3*env.n_riders + 2*env.n_areas*env.n_riders : 4*env.n_areas + 3*env.n_riders + 2*env.n_areas*env.n_riders]
    breakdown["n_late_unassigned"] = state[4*env.n_areas + 3*env.n_riders + 2*env.n_areas*env.n_riders : 5*env.n_areas + 3*env.n_riders + 2*env.n_areas*env.n_riders]
    breakdown["n_late_assigned"] = state[5*env.n_areas + 3*env.n_riders + 2*env.n_areas*env.n_riders : 6*env.n_areas + 3*env.n_riders + 2*env.n_areas*env.n_riders]
    breakdown["time"] = state[-1]

    return breakdown

def explain_state(env):
    breakdown = breakdown_state(env)
    print("------------------------")
    print("n processing:", breakdown["n_processing"])
    print("n unassigned", breakdown["n_unassigned"])
    print("rider in branch:", breakdown["rider_in_branch"])
    print("n assigned:", breakdown["n_assigned"])
    print("rider destinations:", breakdown["rider_destinations"])
    print("earliest unassigned:", breakdown["earliest_unassigned"])
    print("earliest undispatched:", breakdown["earliest_undispatched"])
    print("last dispatch time:", breakdown["last_dispatch_time"])
    print("n late processing:", breakdown["n_late_processing"])
    print("n late unassigned:", breakdown["n_late_unassigned"])
    print("n late assigned:", breakdown["n_late_assigned"])
    print("time:", breakdown["time"])

def threshold_float(float_val, min, max, lower_thresh = 0, upper_thresh = 1):
    if float_val <= lower_thresh:
        return min
    elif float_val >= upper_thresh:
        return max
    else:
        return min + ((float_val - lower_thresh)/(upper_thresh - lower_thresh))*(max - min)

def preprocess_ddpg_action(nn_action, env, **kwargs):
    '''
    Converts the action output of a neural network (from DDPG)
    into the format for the dispatcher environment.

    The neural network outputs values from 0 to 1.

    The environment expects integers for assignment and 0/1 for dispatch.
    '''
    max_assignment = kwargs.get("max_assignment", env.rider_capacity)
    assignment_lower_thresh = kwargs.get("assignment_lower_thresh", 0.2)
    assignment_upper_thresh = kwargs.get("assignment_upper_thresh", 0.8)

    assignment_matrix = nn_action[:-env.n_riders]
    dispatch_vector = nn_action[-env.n_riders:]

    return np.concatenate((
        np.round_([threshold_float(a, 0, max_assignment, assignment_lower_thresh, assignment_upper_thresh) for a in assignment_matrix]),
        np.round_(dispatch_vector)
    )).astype('int32').tolist()
    
def preprocess_dqn_action(dqn_action, env):
    if dqn_action >= env.action_dim:
        # NOTE: it's the do nothing action
        return [0 for i in range(env.action_dim)]
    else:
        return [1 if i == dqn_action else 0 for i in range(env.action_dim)]

class MyPriorityQueue(queue.PriorityQueue):

    def peek(self):
        """Peeks the first element of the queue
        
        Returns
        -------
        item : object
            First item in the queue
        
        Raises
        ------
        queue.Empty
            No items in the queue
        """
        try:
            with self.mutex:
                return self.queue[0]
        except IndexError:
            raise queue.Empty

    def iter_until(self, value):
        """Generates tuples until the given priority value
        
        Yields
        -------
        value: float
            Priority value of the next item
        item : object
            The next item by priority value
        """
        if self.qsize() <= 0:
            more = False
        elif self.peek()[0] > value:
            more = False
        else:
            more = True

        while more:
            next_val, next_item = self.get()

            yield next_val, next_item

            if self.qsize() <= 0:
                more = False
            elif self.peek()[0] > value:
                more = False

class Logger:
    '''
    Log the following events from a DispatcherEnv:
        Arrival
        Dispatch
    '''

    def __init__(self, path = None):
        self.path = path
        
        if self.path is None:
            return None

        # NOTE: I think this empties the file
        f = open(self.path, "w+")
        f.close()

        self.csv_file = open(self.path, "a", newline='')
        self.writer = csv.writer(self.csv_file)
        self.writer.writerow([
            "time",
            "event_type",
            "rider",
            "n_carried_before",
            "n_carried_after",
            "n_delivered", # NOTE: only for arrival
            "origin",
            "destination",
            "lateness", # NOTE: only for arrival
            "path", # NOTE: only for dispatch
        ])

    def close(self):
        if self.path is None:
            return False

        self.csv_file.close()

    def log(self, **kwargs):
        if self.path is None:
            return False

        time = kwargs.get("time", None)
        event_type = kwargs.get("event_type", None)
        rider = kwargs.get("rider", None)
        n_carried_before = kwargs.get("n_carried_before", None)
        n_carried_after = kwargs.get("n_carried_after", None)
        n_delivered = kwargs.get("n_delivered", None)
        origin = kwargs.get("origin", None)
        destination = kwargs.get("destination", None)
        lateness = kwargs.get("lateness", None)
        path = kwargs.get("path", None)

        self.writer.writerow([
            time,
            event_type,
            rider,
            n_carried_before,
            n_carried_after,
            n_delivered,
            origin,
            destination,
            ','.join(str(l) for l in lateness) if isinstance(lateness, list) else None,
            ','.join(str(d) for d in path) if isinstance(path, list) else None,
        ])

class LinearDecaySchedule:
    '''
    Utility for decaying a value in a linear pattern:

    val(t) = val(t - 1) + increment
    '''

    def __init__(self, start_val, end_val, end_decay, start_decay = 0):
        self.start_val = start_val
        self.end_val = end_val
        self.end_decay = end_decay

        self._increment = (self.start_val - self.end_val)/(start_decay - self.end_decay)

    def get_val(self, time_period):
        if time_period > self.end_decay:
            return self.end_val
        else:
            return self.start_val + self._increment*time_period

class ExponentialDecaySchedule:
    '''
    Utility for decaying a value in an exponential pattern:

    val(t) = val(t - 1)*factor
    '''

    def __init__(self, start_val, end_val, end_decay, start_decay = 0):
        self.start_val = start_val
        self.end_val = end_val
        self.end_decay = end_decay

        self.len_decay = self.end_decay - start_decay

        # self._increment = (self.start_val - self.end_val)/(start_decay - self.end_decay)

        self._factor = (self.end_val/self.start_val)**(1/self.len_decay)

    def get_val(self, time_period):
        if time_period > self.end_decay:
            return self.end_val
        else:
            return self.start_val*(self._factor**time_period)