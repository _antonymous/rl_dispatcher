'''
Initialize some default variables for testing agents
and the environment
'''
import os
import numpy as np

SEED = 1
N_EPISODES = 5000
N_EPISODES_TEST = 100
WARMUP_EPISODES = 50
EPISODE_LEN = 60*24
REWARD_AGG_INTERVAL = 50
N_RIDERS = 3
DIST_MAT = np.matrix([
    [0, 10, 15, 20],
    [12, 0, 8, 8],
    [15, 7, 0, 12],
    [20, 8, 14, 0],
])
ORDER_RATES = [20, 16, 24]
N_AREAS = DIST_MAT.shape[0] - 1

RIDER_CAPACITY = 3 # NOTE: this acts as max for assignments
TRAIN_INTERVAL = 1
EXPLORATION_NOISE = 1 # NOTE: I'll just use this for both assignments and dispatching

MIN_ACTIONS = [0 for i in range(N_RIDERS*N_AREAS + N_RIDERS)]
MAX_ACTIONS = [RIDER_CAPACITY for i in range(N_RIDERS*N_AREAS)] + [1 for i in range(N_RIDERS)]
