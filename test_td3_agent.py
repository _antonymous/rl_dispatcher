'''
Run some descriptive tests on the TD3 agent
'''
import os
import random
from collections import deque, namedtuple
import numpy as np
from tqdm import tqdm
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.autograd import Variable

from agents import TD3Agent, HeuristicAgent
from environments import DispatcherEnv
from utils import Transition, breakdown_state
from defaults import SEED, N_EPISODES_TEST, WARMUP_EPISODES, EPISODE_LEN
from defaults import REWARD_AGG_INTERVAL, N_RIDERS, DIST_MAT, ORDER_RATES
from defaults import N_AREAS, RIDER_CAPACITY, TRAIN_INTERVAL, EXPLORATION_NOISE
from defaults import MIN_ACTIONS, MAX_ACTIONS

USE_CUDA = torch.cuda.is_available()
FloatTensor = torch.cuda.FloatTensor if USE_CUDA else torch.FloatTensor
LongTensor = torch.cuda.LongTensor if USE_CUDA else torch.LongTensor
ByteTensor = torch.cuda.ByteTensor if USE_CUDA else torch.ByteTensor
DEVICE = torch.device("cuda" if USE_CUDA else "cpu")

MIN_ACTIONS_TENSOR = FloatTensor(MIN_ACTIONS)
MAX_ACTIONS_TENSOR = FloatTensor(MAX_ACTIONS)

def main():
    random.seed(SEED)
    np.random.seed(SEED)
    torch.manual_seed(SEED)

    env = DispatcherEnv(
        DIST_MAT, 
        N_RIDERS,
        betas = ORDER_RATES,
        episode_length = EPISODE_LEN,
        rider_capacity = RIDER_CAPACITY,
    )

    td3_agent = TD3Agent(
        env.state_dim,
        env.action_dim,
        policy_noise = FloatTensor([0.5 for i in range(N_RIDERS*N_AREAS)] + [0.5 for i in range(N_RIDERS)]),
        noise_clip = FloatTensor([1 for i in range(N_RIDERS*N_AREAS)] + [1 for i in range(N_RIDERS)]),
        min_actions = MIN_ACTIONS_TENSOR,
        max_actions = MAX_ACTIONS_TENSOR,
    )
    td3_agent.load(os.path.join("trained_agents", "dispatcher_td3", '_final'))

    for episode in tqdm(range(N_EPISODES_TEST), ascii = True, unit = "episode"):
        episode_reward = 0
        lateness_penalty = 0
        earliness_reward = 0
        travel_penalty = 0
        ontime_deliveries = 0
        late_deliveries = 0

        step = 0

        while not is_done:
            action = agent_.get_action(current_state).numpy()
            action = np.round_(action).astype('int32')

            next_state, reward, is_done, info = env.step(action)

            episode_reward += reward
            lateness_penalty += info["lateness_penalty"]
            earliness_reward += info["early_reward"]
            travel_penalty += info["travel_penalty"]
            ontime_deliveries += sum(info["ontime_deliveries"])
            late_deliveries += sum(info["late_deliveries"])

            current_state = FloatTensor(next_state)

            step += 1

    # what to plot?

    # TODO: orders and dispatchments over time?

    # TODO: number of orders per dispatchment?

    # TODO: 

if __name__ == "__main__":
    main()