'''
Try a heuristic agent to compare performance to RL agents.
'''
import os
import random
from collections import deque, namedtuple
import numpy as np
from tqdm import tqdm
from copy import copy
import pickle
import json

from agents import HeuristicAgent
from environments import DispatcherEnv
from utils import breakdown_state
from defaults import SEED, N_EPISODES, WARMUP_EPISODES, EPISODE_LEN
from defaults import REWARD_AGG_INTERVAL, N_RIDERS, DIST_MAT, ORDER_RATES
from defaults import N_AREAS, RIDER_CAPACITY, TRAIN_INTERVAL, EXPLORATION_NOISE
from defaults import MIN_ACTIONS, MAX_ACTIONS

class Individual:

    def __init__(self, attribute_vector):
        self.attribute_vector = attribute_vector
        self.fitness = None

class DispatcherGeneticAlgorithm:
    '''
    NOTE: An individual is [travel time buffer for each area] + [max_destinations]
    '''

    def __init__(self, env, **kwargs):
        self.env = env
        self.generation_size = kwargs.get("generation_size", 100)
        self.n_generations = kwargs.get("n_generations", 100)
        self.mutate_prob = kwargs.get("mutate_prob", 0.05)
        self.evaluation_sample_size = kwargs.get("evaluation_sample_size", 3)
        self.verbosity = kwargs.get("verbosity", 0)
        
        self.max_times = np.array([max(self.env.dist_mat[:, i + 1]) for i in range(self.env.n_areas)])

    def _random_individual(self):
        '''
        NOTE: Let travel time buffers be a random float between
            (-max_expected_time_to_dest, +max_expected_time_to_dest)
        NOTE: Let max_destinations be random int between 1 and self.n_areas
        '''

        travel_time_buffers = np.random.uniform(-self.max_times, self.max_times)
        max_destinations = np.random.randint(1, self.env.n_areas + 1)

        return Individual(travel_time_buffers.flatten().tolist() + [max_destinations])

    def _evaluate_individual(self, individual):
        '''
        Construct the agent corresponding to the individual.
        Run the agent for self.evaluation_sample_size episodes.
        Take the average cost as the individual's fitness.
        '''
        agent = HeuristicAgent(
            self.env.state_dim,
            self.env.action_dim,
            n_riders = N_RIDERS,
            dist_mat = DIST_MAT,
            rider_capacity = RIDER_CAPACITY,
            travel_time_buffer = individual.attribute_vector[:self.env.n_areas],
            max_destinations = individual.attribute_vector[-1],
        )

        episodes_rewards = []

        for episode in range(self.evaluation_sample_size):
            episode_reward = 0
            step = 0
            current_state = self.env.reset()
            is_done = False

            while not is_done:
                action = agent.get_action(breakdown_state(self.env))

                next_state, reward, is_done, info = self.env.step(action)

                episode_reward += reward
                current_state = next_state
                step += 1
            
            episodes_rewards.append(episode_reward)
        
        return np.mean(episodes_rewards)

    def _select_parents(self, population):
        '''
        From the population, select 2 parents.
        Give higher probability to individuals with higher fitness.
        '''
        pop_fitness = [idv.fitness for idv in population]
        min_fitness = min(pop_fitness)
        max_fitness = max(pop_fitness)
        range_fitness = max_fitness - min_fitness
        if range_fitness <= 0:
            return np.random.choice(population, size = 2, replace = False)

        scaled_fitness = [(f - min_fitness)/range_fitness for f in pop_fitness]
        adjustment_1 = abs(min(scaled_fitness))
        adjustment_2 = 0.01*abs(np.mean(scaled_fitness))
        adjusted_fitness = [sf + adjustment_1 + adjustment_2 for sf in scaled_fitness]
        total_af = sum(adjusted_fitness)
        probs = [af/total_af for af in adjusted_fitness]

        return np.random.choice(population, size = 2, replace = False, p=probs)

    def _mate(self, parent1, parent2):
        '''
        Randomly take each number from one of the parents.
        '''
        new_attribute_vector = []

        for i in range(self.env.n_areas + 1):
            if np.random.randint(0, 2) < 1:
                source_parent = parent1
            else:
                source_parent = parent2

            new_attribute_vector.append(copy(source_parent.attribute_vector[i]))
        
        return Individual(new_attribute_vector)

    def _mutate(self, individual):
        '''
        NOTE: assign a new value to a random item in the vector
        NOTE: let the value be a random float between the limits of that item
        NOTE: if the item is max_destinations, round to integer
        NOTE: clip the final values to the same limits as self._random_individual
        '''
        mutation_idx = np.random.randint(0, self.env.n_areas + 1)

        if mutation_idx == self.env.n_areas:
            # NOTE: it's max_destinations
            new_val = np.random.randint(1, self.env.n_areas + 1)
        else:
            # NOTE: it's one of the travel_time_buffers
            new_val = np.random.uniform(-self.max_times[mutation_idx], self.max_times[mutation_idx])
        
        new_attribute_vector = copy(individual.attribute_vector)
        new_attribute_vector[mutation_idx] = new_val

        return Individual(new_attribute_vector)

    def _get_survivors(self, population):
        '''
        Select self.generation_size survivors.
        Give higher probability to individuals with higher fitness.
        '''
        pop_fitness = [idv.fitness for idv in population]
        min_fitness = min(pop_fitness)
        max_fitness = max(pop_fitness)
        range_fitness = max_fitness - min_fitness
        if range_fitness <= 0:
            return np.random.choice(population, size = self.generation_size, replace = False)

        scaled_fitness = [(f - min_fitness)/range_fitness for f in pop_fitness]
        adjustment_1 = abs(min(scaled_fitness))
        adjustment_2 = 0.01*abs(np.mean(scaled_fitness))
        adjusted_fitness = [sf + adjustment_1 + adjustment_2 for sf in scaled_fitness]
        total_af = sum(adjusted_fitness)
        probs = [af/total_af for af in adjusted_fitness]

        return np.random.choice(population, size = self.generation_size, replace = False, p=probs)

    def run(self):
        self.best_individual = None
        self.fitness_ascent = []

        population = []
        for i in range(self.generation_size):
            new_individual = self._random_individual()
            new_individual.fitness = self._evaluate_individual(new_individual)

            if self.best_individual is None:
                self.best_individual = copy(new_individual)
            elif new_individual.fitness > self.best_individual.fitness:
                self.best_individual = copy(new_individual)

            population = np.append(population, new_individual)

        self.fitness_ascent.append(copy(self.best_individual.fitness))

        for n_gen in range(self.n_generations):
            parent_population = copy(population)

            for i in range(self.generation_size):
                parent1, parent2 = self._select_parents(population)
                new_individual = self._mate(parent1, parent2)

                if np.random.rand() < self.mutate_prob:
                    new_individual = self._mutate(new_individual)

                new_individual.fitness = self._evaluate_individual(new_individual)

                if new_individual.fitness > self.best_individual.fitness:
                    self.best_individual = copy(new_individual)

                population = np.append(population, new_individual)
            
            population = self._get_survivors(population)
            self.fitness_ascent.append(copy(self.best_individual.fitness))

            if self.verbosity >= 1:
                print("generation:", n_gen, "best fitness:", self.best_individual.fitness)

    def get_best_agent(self):
        '''
        NOTE: Return an agent, not a vector or individual
        '''
        return HeuristicAgent(
            self.env.state_dim,
            self.env.action_dim,
            n_riders = N_RIDERS,
            dist_mat = DIST_MAT,
            rider_capacity = RIDER_CAPACITY,
            travel_time_buffer = self.best_individual.attribute_vector[:self.env.n_areas],
            max_destinations = self.best_individual.attribute_vector[-1],
        )

def main():

    random.seed(SEED)
    np.random.seed(SEED)

    env = DispatcherEnv(
        DIST_MAT, 
        N_RIDERS,
        betas = ORDER_RATES,
        episode_length = EPISODE_LEN,
        rider_capacity = RIDER_CAPACITY,
    )

    ga = DispatcherGeneticAlgorithm(
        env,
        n_generations = 100,
        evaluation_sample_size = 10,
        verbosity = 1,
    )

    ga.run()

    best_agent = ga.get_best_agent()
    best_agent.save("./heuristic_models/ga_agent")

def load_and_test():
    random.seed(SEED)
    np.random.seed(SEED)

    env = DispatcherEnv(
        DIST_MAT, 
        N_RIDERS,
        betas = ORDER_RATES,
        episode_length = EPISODE_LEN,
        rider_capacity = RIDER_CAPACITY,
    )

    # NOTE: GA agent
    # agent = HeuristicAgent(
    #     env.state_dim,
    #     env.action_dim,
    #     n_riders = N_RIDERS,
    #     dist_mat = DIST_MAT,
    #     rider_capacity = RIDER_CAPACITY,
    # )
    # agent.load("./heuristic_models/ga_agent")

    # NOTE: timeliness agent
    agent = HeuristicAgent(
        env.state_dim,
        env.action_dim,
        n_riders = N_RIDERS,
        dist_mat = DIST_MAT,
        rider_capacity = RIDER_CAPACITY,
        travel_time_buffer = [10,10,10],
        max_destinations = 1,
    )

    # NOTE: efficiency agent
    # agent = HeuristicAgent(
    #     env.state_dim,
    #     env.action_dim,
    #     n_riders = N_RIDERS,
    #     dist_mat = DIST_MAT,
    #     rider_capacity = RIDER_CAPACITY,
    #     travel_time_buffer = [0,0,0],
    #     max_destinations = 3,
    # )

    print("Travel time buffer:", agent.travel_time_buffer)
    print("Max destinations:", agent.max_destinations)

    episode_test_size = 100
    episode_rewards = []
    episode_lateness_penalties = []
    episode_earliness_rewards = []
    episode_travel_penalties = []

    for episode in tqdm(range(episode_test_size), ascii = True, unit = "episode"):

        # NOTE: just trying one episode
        episode_reward = 0
        lateness_penalty = 0
        earliness_reward = 0
        travel_penalty = 0

        step = 0
        current_state = env.reset()
        is_done = False

        while not is_done:
            action = agent.get_action(breakdown_state(env))

            next_state, reward, is_done, info = env.step(action)

            episode_reward += reward
            lateness_penalty += info["lateness_penalty"]
            earliness_reward += info["early_reward"]
            travel_penalty += info["travel_penalty"]

            current_state = next_state
            step += 1
        
        episode_rewards.append(episode_reward)
        episode_lateness_penalties.append(lateness_penalty)
        episode_earliness_rewards.append(earliness_reward)
        episode_travel_penalties.append(travel_penalty)

    print("Mean Episode Reward:", np.mean(episode_rewards))
    print("Median Episode Reward:", np.median(episode_rewards))
    print("Mean Lateness Penalties:", np.mean(episode_lateness_penalties))
    print("Mean Earliness Rewards:", np.mean(episode_earliness_rewards))
    print("Mean Travel Penalties:", np.mean(episode_travel_penalties))

def try_agent():
    import matplotlib.pyplot as plt

    random.seed(SEED)
    np.random.seed(SEED)

    env = DispatcherEnv(
        DIST_MAT, 
        N_RIDERS,
        betas = ORDER_RATES,
        episode_length = EPISODE_LEN,
        rider_capacity = RIDER_CAPACITY,
    )

    agent = HeuristicAgent(
        env.state_dim,
        env.action_dim,
        n_riders = N_RIDERS,
        dist_mat = DIST_MAT,
        rider_capacity = RIDER_CAPACITY,
    )

    agent.load("./heuristic_models/ga_agent.pkl")

    # NOTE: just trying one episode
    episode_reward = 0
    step = 0
    current_state = env.reset()
    is_done = False

    # NOTE: track some metrics to plot later
    ep_assignments = []
    ep_dispatchments = []
    ep_early_rewards = []
    ep_lateness_penalties = []
    ep_ontime_deliveries = []
    ep_late_deliveries = []
    ep_system_deliveries = []

    while not is_done:

        action = agent.get_action(breakdown_state(env))

        next_state, reward, is_done, info = env.step(action)

        # NOTE: track some metrics to plot later
        ep_assignments.append(sum([sum(alist) for alist in info["made_assignments"]]))
        ep_dispatchments.append(sum(info["made_dispatchments"]))
        ep_early_rewards.append(info["early_reward"])
        ep_lateness_penalties.append(info["lateness_penalty"])
        ep_ontime_deliveries.append(sum(info["ontime_deliveries"]))
        ep_late_deliveries.append(sum(info["late_deliveries"]))
        ep_system_deliveries.append(sum(info["deliveries_in_system"]))

        episode_reward += reward
        current_state = next_state
        step += 1

    print(episode_reward)

    plt.plot(ep_assignments)
    plt.title("Episode Assignments")
    plt.show()
    plt.clf()

    plt.plot(ep_dispatchments)
    plt.title("Episode Dispatchments")
    plt.show()
    plt.clf()

    plt.plot(ep_early_rewards)
    plt.title("Episode Early Rewards")
    plt.show()
    plt.clf()

    plt.plot(ep_lateness_penalties)
    plt.title("Episode Lateness Penalties")
    plt.show()
    plt.clf()

    plt.plot(ep_ontime_deliveries)
    plt.title("Episode On Time deliveries")
    plt.show()
    plt.clf()

    plt.plot(ep_late_deliveries)
    plt.title("Episode Late deliveries")
    plt.show()
    plt.clf()

    plt.plot(ep_system_deliveries)
    plt.title("Episode Deliveries in system")
    plt.show()
    plt.clf()

if __name__ == "__main__":
    main()
    # load_and_test()
    # try_agent()