import os
import random
from collections import deque, namedtuple
import numpy as np
from tqdm import tqdm
from agents import TD3Agent

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.autograd import Variable

from environments import DispatcherEnv
from utils import Transition
from defaults import SEED, N_EPISODES, WARMUP_EPISODES, EPISODE_LEN
from defaults import REWARD_AGG_INTERVAL, N_RIDERS, DIST_MAT, ORDER_RATES
from defaults import N_AREAS, RIDER_CAPACITY, TRAIN_INTERVAL, EXPLORATION_NOISE
from defaults import MIN_ACTIONS, MAX_ACTIONS

USE_CUDA = torch.cuda.is_available()
FloatTensor = torch.cuda.FloatTensor if USE_CUDA else torch.FloatTensor
LongTensor = torch.cuda.LongTensor if USE_CUDA else torch.LongTensor
ByteTensor = torch.cuda.ByteTensor if USE_CUDA else torch.ByteTensor
DEVICE = torch.device("cuda" if USE_CUDA else "cpu")

MIN_ACTIONS_TENSOR = FloatTensor(MIN_ACTIONS)
MAX_ACTIONS_TENSOR = FloatTensor(MAX_ACTIONS)

if USE_CUDA:
    print("Using CUDA")
else:
    print("Using CPU")

def main():
    random.seed(SEED)
    np.random.seed(SEED)
    torch.manual_seed(SEED)

    env = DispatcherEnv(
        DIST_MAT, 
        N_RIDERS,
        betas = ORDER_RATES,
        episode_length = EPISODE_LEN,
        rider_capacity = RIDER_CAPACITY,
    )

    agent = TD3Agent(
        env.state_dim,
        env.action_dim,
        policy_noise = FloatTensor([0.5 for i in range(N_RIDERS*N_AREAS)] + [0.5 for i in range(N_RIDERS)]),
        noise_clip = FloatTensor([1 for i in range(N_RIDERS*N_AREAS)] + [1 for i in range(N_RIDERS)]),
        min_actions = MIN_ACTIONS_TENSOR,
        max_actions = MAX_ACTIONS_TENSOR,
    )

    recent_episode_rewards = deque(maxlen = REWARD_AGG_INTERVAL)

    for episode in tqdm(range(N_EPISODES), ascii = True, unit = "episode"):

        episode_reward = 0
        step = 0
        current_state = FloatTensor(env.reset())
        is_done = False

        while not is_done:
            if episode > WARMUP_EPISODES:
                action = agent.get_action(current_state).numpy()
                action = action + np.random.normal(
                    0,
                    EXPLORATION_NOISE,
                    size = env.action_dim,
                )
                action = action.clip(MIN_ACTIONS, MAX_ACTIONS)
            else:
                action = np.random.uniform(MIN_ACTIONS, MAX_ACTIONS)

            action = np.round_(action).astype('int32')

            next_state, reward, is_done, info = env.step(action)

            next_state = FloatTensor(next_state)
            action = FloatTensor(action)
            reward = FloatTensor([reward]).view(1, 1)
            done_ = FloatTensor([1 if is_done else 0]).view(1, 1)

            episode_reward += reward.item()

            agent.update_replay_memory(Transition(
                current_state,
                action,
                next_state,
                reward,
                done_,
            ))

            if (episode*EPISODE_LEN + step) % TRAIN_INTERVAL == 0:
                agent.train()

            current_state = next_state
            step += 1

        recent_episode_rewards.append(episode_reward)

        if episode % REWARD_AGG_INTERVAL == 0:
            average_reward = np.mean(recent_episode_rewards)
            median_reward = np.median(recent_episode_rewards)
            min_reward = min(recent_episode_rewards)
            max_reward = max(recent_episode_rewards)

            print("------------------")
            print("Episode:", episode)
            print("Reward Min:", min_reward, "Median:", median_reward, "Avg:", average_reward, "Max:", max_reward)

    # TODO: save model
    agent.save(os.path.join("torch_models", "dispatcher_td3", "_" + 'final'))

def load_and_test():
    random.seed(SEED)
    np.random.seed(SEED)
    torch.manual_seed(SEED)

    env = DispatcherEnv(
        DIST_MAT, 
        N_RIDERS,
        betas = ORDER_RATES,
        episode_length = EPISODE_LEN,
        rider_capacity = RIDER_CAPACITY,
    )

    agent = TD3Agent(
        env.state_dim,
        env.action_dim,
        policy_noise = FloatTensor([0.5 for i in range(N_RIDERS*N_AREAS)] + [0.5 for i in range(N_RIDERS)]),
        noise_clip = FloatTensor([1 for i in range(N_RIDERS*N_AREAS)] + [1 for i in range(N_RIDERS)]),
        min_actions = MIN_ACTIONS_TENSOR,
        max_actions = MAX_ACTIONS_TENSOR,
    )

    agent.load(os.path.join("torch_models", "dispatcher_td3", "_" + 'final'))

    episode_test_size = 100
    episode_rewards = []
    episode_lateness_penalties = []
    episode_earliness_rewards = []
    episode_travel_penalties = []

    for episode in tqdm(range(episode_test_size), ascii = True, unit = "episode"):

        episode_reward = 0
        lateness_penalty = 0
        earliness_reward = 0
        travel_penalty = 0

        step = 0
        current_state = FloatTensor(env.reset())
        is_done = False

        while not is_done:
            action = agent.get_action(current_state).numpy()
            action = np.round_(action).astype('int32')

            next_state, reward, is_done, info = env.step(action)

            next_state = FloatTensor(next_state)

            episode_reward += reward
            lateness_penalty += info["lateness_penalty"]
            earliness_reward += info["early_reward"]
            travel_penalty += info["travel_penalty"]

            current_state = next_state
            step += 1
        
        episode_rewards.append(episode_reward)
        episode_lateness_penalties.append(lateness_penalty)
        episode_earliness_rewards.append(earliness_reward)
        episode_travel_penalties.append(travel_penalty)

    print("Mean Episode Reward:", np.mean(episode_rewards))
    print("Median Episode Reward:", np.median(episode_rewards))
    print("Mean Lateness Penalties:", np.mean(episode_lateness_penalties))
    print("Mean Earliness Rewards:", np.mean(episode_earliness_rewards))
    print("Mean Travel Penalties:", np.mean(episode_travel_penalties))

if __name__ == "__main__":
    main()
    # load_and_test()