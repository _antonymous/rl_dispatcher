import os
import numpy as np
from collections import deque, namedtuple
from copy import copy
import uuid

from utils import MyPriorityQueue, Logger

Delivery = namedtuple('Delivery', (
    'id',
    'order_time',
    'destination',
))

Arrival = namedtuple('Arrival', (
    'rider_idx',
    'destination',
    'departure_time',
))

class Rider:

    def __init__(self, capacity = 1):
        self.capacity = capacity

        self.n_deliveries = 0
        self.deliveries = {}
            # NOTE: {area_idx: [deliveries]}

        self.route = (None, None)
            # NOTE: This indicates the origin and destination when the rider is enroute
            # NOTE: indexed same as distance matrix, with branch as 0

        self.path = []
            # NOTE: This is the sequence of destinations for this trip
            # NOTE: Does not include the return to the branch

    def assign_delivery(self, delivery):
        if self.n_deliveries >= self.capacity:
            return False
        else:
            self.n_deliveries += 1
            self.deliveries[delivery.destination] = self.deliveries.get(delivery.destination, [])
            self.deliveries[delivery.destination].append(delivery)
            return True

def tsp_greedy(destinations, dist_mat):
    '''
    Provides a path passing all destinations by following a greedy approach.
    NOTE: distance matrix has the branch at index 0
    NOTE: destinations are indexed the same way, with the branch
    '''
    remaining_destinations = copy(destinations)
    path = []
    last_loc = 0
    while len(remaining_destinations) > 0:
        distances = [dist_mat[last_loc, dest] for dest in remaining_destinations]
        next_dest = remaining_destinations[np.argmin(distances)]
        path.append(next_dest)
        remaining_destinations.remove(next_dest)
        last_loc = next_dest
    return path

# TODO: prioritize path by due time, not by greedy
def tsp_by_time(destinations, due_time, dist_mat, current_time):
    '''
    If earliest delivery won't make it based on expected time, prioritize that destination
    else use greedy?
    '''
    remaining_destinations = copy(destinations)
    remaining_due_times = copy(due_time)
    expected_depart_time = copy(current_time)

    path = []
    last_loc = 0
    while len(remaining_destinations) > 0:

        prioritize_due = False
        expected_lateness = []

        for dest_idx, dest in enumerate(remaining_destinations):
            exp_lateness = (expected_depart_time + dist_mat[last_loc, dest]) - remaining_due_times[dest_idx]
            expected_lateness.append(exp_lateness)
            if exp_lateness >= 0:
                prioritize_due = True
                
        if prioritize_due:
            # prioritize by due time
            next_dest_idx = np.argmax(expected_lateness)
            next_dest = remaining_destinations[next_dest_idx]
        else:
            # prioritize by greedy (closest)
            distances = [dist_mat[last_loc, dest] for dest in remaining_destinations]
            next_dest_idx = np.argmin(distances)
            next_dest = remaining_destinations[next_dest_idx]

        path.append(next_dest)
        remaining_destinations.pop(next_dest_idx)
        remaining_due_times.pop(next_dest_idx)
        expected_depart_time += dist_mat[last_loc, next_dest]
        last_loc = next_dest
        
    return path

class DispatcherEnv:
    '''
    Environment for a delivery dispatch system.
    Dispatcher has a single branches.
    There are multiple destinations for deliveries.
    There is a matrix indicating the distance between each pair of locations (branche and destinations).
    Each step moves one time period (such as a minute).
    Each destination orders deliveries following a Poisson distribution. (This can be varied later on)
    Each order is expected to be delivered within a specified time.
    Riders are stationed at the branch by default.
    Riders have a fixed capacity for number of deliveries each trip.
    Riders use a specified heuristic for determining the order in which they fulfill deliveries. (Later on, this can be another RL agent)
    After finishing deliveries, riders go to the nearest branch.
    Internally, deliveries are tracked individually in order to check order and fulfillment time.
    The dispatcher is rewarded for on-time deliveries and penalized for late deliveries.
    Delivery transactions are instanteneous.

    Notes:
        Make time periods long so that time spent for actions is minimal.
    '''

    def __init__(self, dist_mat, n_riders, **kwargs):
        '''
        
        Notes:
            The first index on the distance matrix is assumed to be the branch
            All other rows and columns are assumed to refer to destinations
        '''
        self.dist_mat = dist_mat
        self.n_riders = n_riders
        self.n_areas = self.dist_mat.shape[0] - 1

        self.betas = kwargs.get("betas", [10 for i in range(self.n_areas)])
            # NOTE: the mean of the exponential distribution for delivery orders for each destination
        self.rider_capacity = kwargs.get("rider_capacity", 3)
        self.episode_length = kwargs.get("episode_length", 60*24)

        self.delivery_lead_time = kwargs.get("delivery_lead_time", 45)
        self.late_penalty_dt = kwargs.get("late_penalty_dt", 1)
            # NOTE: penalty per time unit a delivery is late
            # NOTE: can add a fixed penalty if needed
        self.early_reward_dt = kwargs.get("early_reward_dt", 0.25)
            # NOTE: reward per time unit a delivery is early
            # NOTE: can add a fixed reward if needed
        self.travel_penalty_dt = kwargs.get("travel_penalty_dt", 0.25)
            # NOTE: penalty for every 1 time unit a rider travels

        self.processing_delay = kwargs.get("processing_delay", 5)

        self.deterministic_travel_time = kwargs.get("deterministic_travel_time", False)

        self.deterministic_processing_time = kwargs.get("deterministic_processing_time", False)



        self.log_path = kwargs.get("log_path", None)

        self.reset()

        self.state_dim = len(self.get_state())
        self.action_dim = self.n_riders*self.n_areas + self.n_riders

    def reset(self):
        self.time = 0
        self.is_done = False

        self.logger = Logger(path = self.log_path)

        self.riders = [Rider(capacity = self.rider_capacity) for i in range(self.n_riders)]
        
        self._next_deliveries = MyPriorityQueue()
            # NOTE: internal variable not included in state
            # NOTE: next orders which will arrive based on exponential distributions

        # NOTE: produce first set of deliveries
        for area_idx in range(1, self.n_areas + 1):
            # NOTE: betas are indexed without the branch
            next_order_time = self.time + np.random.exponential(
                self.betas[area_idx - 1]
            )

            self._next_deliveries.put((
                next_order_time,
                Delivery(
                    uuid.uuid4(),
                    next_order_time,
                    area_idx,
                )
            ))

        self._next_arrivals = MyPriorityQueue()
            # NOTE: internal variable
            # NOTE: stores arrival times of riders to their next destination

        self.processing_deliveries = MyPriorityQueue()
            # NOTE: WIP deliveries, prioritized by when the processing will finish

        self.unassigned_deliveries = [[] for i in range(self.n_areas)]
            # NOTE: orders that have not been assigned to any rider

        # NOTE: track state components for performance
        self.n_processing = [0 for i in range(self.n_areas)]
        self.n_unassigned = [0 for i in range(self.n_areas)]
        self.rider_in_branch = [1 for j in range(self.n_riders)]
        self.n_assigned = [[0 for j in range(self.n_riders)] for i in range(self.n_areas)]
        self.rider_destinations = [0 for j in range(self.n_riders)]
        self.last_dispatch_time = [self.episode_length*2 for j in range(self.n_riders)]
        self.earliest_unassigned = [self.episode_length*2 for i in range(self.n_areas)]

        # NOTE: maybe make this per rider also?
        # self.earliest_undispatched = [self.episode_length*2 for i in range(self.n_areas)]
        self.earliest_undispatched = [[self.episode_length*2 for j in range(self.n_riders)] for i in range(self.n_areas)]

        self.n_late_processing = [0 for i in range(self.n_areas)]
        self.n_late_unassigned = [0 for i in range(self.n_areas)]
        self.n_late_assigned = [0 for i in range(self.n_areas)]

        return self.get_state()

    def step(self, action):
        # NOTE: check if actions are correct? (integers, binaries) or transform outside the agent?
        # NOTE: update state components on-the-fly
        # NOTE: action vector is:
            # [[(# of deliveries to area i to assign to rider j) for j in riders] for i in areas]
            # + [(1 if dispatch rider j, 0 otherwise) for j in riders]

        # NOTE: review to ensure that area indexing is correct
            # destinations indexed at 0, except in dist_mat where branch is 0

        # NOTE: penalize lateness for undelivered deliveries. 
            # so that the agent does not learn not to deliver

        assert len(action) == self.n_areas*self.n_riders + self.n_riders

        # NOTE: initialize reward for this step
        reward = 0
        lateness_penalty = 0
        travel_penalty = 0
        early_reward = 0

        # NOTE: if episode is done, don't allow another step
        if self.is_done:
            return self.get_state(), reward, self.is_done, None

        # NOTE: track on time and late deliveries for extra info
        ontime_deliveries = [0 for i in range(self.n_areas)]
        late_deliveries = [0 for i in range(self.n_areas)]

        for arrival_time, arrival in self._next_arrivals.iter_until(self.time):

            rider = self.riders[arrival.rider_idx]

            # NOTE: remove first item in rider.path
            current_dest = self.riders[arrival.rider_idx].path.pop(0)

            # NOTE: update rider route
            if len(self.riders[arrival.rider_idx].path) <= 0:
                self.riders[arrival.rider_idx].route = (current_dest, 0)
            else:
                self.riders[arrival.rider_idx].route = (current_dest, self.riders[arrival.rider_idx].path[0])

            # NOTE: apply penalty for travel
            # NOTE: penalty is applied upon arrival
            travel_penalty += self.travel_penalty_dt*(self.time - arrival.departure_time)

            n_carried_before = rider.n_deliveries
            all_lateness = []

            # NOTE: if arriving at an area
            if arrival.destination > 0:

                # NOTE: check next destination in rider.path
                # NOTE: this should be indexed same as dist_mat
                next_dest = rider.path[0]
                    
                # NOTE: compute time to next destination
                # NOTE: current_dest & next_dest here is indexed same as dist_mat

                if not self.deterministic_travel_time:
                    arrival_time_next = self.time + np.random.exponential(
                        self.dist_mat[current_dest, next_dest]
                    )
                else:
                    arrival_time_next = self.time + self.dist_mat[current_dest, next_dest]
                
                # NOTE: add arrival_event to self._next_arrivals
                self._next_arrivals.put((
                    arrival_time_next,
                    Arrival(
                        arrival.rider_idx,
                        next_dest,
                        copy(self.time),
                    )
                ))

                # NOTE: update self.rider_destinations
                self.rider_destinations[arrival.rider_idx] = next_dest

                # NOTE: For each delivery in this destination
                for delivery in rider.deliveries[current_dest]:
                    # NOTE: Check delivery order_time + self.delivery_lead_time to check if delivery is late or early
                    lateness = self.time - (delivery.order_time + self.delivery_lead_time)
                    all_lateness.append(lateness)

                    # NOTE: update rider.n_deliveries
                    rider.n_deliveries -= 1

                    if lateness > 0:
                        # NOTE: Late
                        # NOTE: apply penalty

                        # NOTE: will apply late penalty outside, even if delivery was not made

                        # reward -= abs(lateness)*self.late_penalty_dt
                        late_deliveries[arrival.destination - 1] += 1

                    else:
                        # NOTE: On time or early
                        # NOTE: apply reward
                        early_reward += abs(lateness)*self.early_reward_dt

                        ontime_deliveries[arrival.destination - 1] += 1

                    # NOTE: update self.n_assigned
                    # NOTE: indexed without branch
                    self.n_assigned[current_dest - 1][arrival.rider_idx] -= 1

                # NOTE: remove all deliveries at this destination
                rider.deliveries[current_dest] = []

            # NOTE: if arriving branch
            else:
                # NOTE: no need to update self.rider_destinations, it should already be 0
                # NOTE: update self.rider_in_branch
                self.rider_in_branch[arrival.rider_idx] = 1

            n_carried_after = rider.n_deliveries

            # NOTE: log arrival
            self.logger.log(
                time = self.time,
                event_type = "arrival",
                rider = arrival.rider_idx,
                n_carried_before = n_carried_before,
                n_carried_after = n_carried_after,
                n_delivered = n_carried_before - n_carried_after,
                origin = None,
                destination = arrival.destination,
                lateness = all_lateness,
                path = None,
            )
                
        # NOTE: receive any new delivery orders at branch
        for order_time, order in self._next_deliveries.iter_until(self.time):

            # NOTE: add some delay for in-branch processing

            # NOTE: determine delivery processing time
            if not self.deterministic_processing_time:
                processing_time = np.random.exponential(self.processing_delay)
            else:
                processing_time = self.processing_delay

            # NOTE: add delivery to self.processing_deliveries
            self.processing_deliveries.put((
                self.time + processing_time,
                order
            ))

            # NOTE: update self.n_processing
            # NOTE: indexed without branch
            self.n_processing[order.destination - 1] += 1

            # NOTE: determine order time of next delivery from the same area
            # NOTE: betas are indexed without branch
            next_order_time = self.time + np.random.exponential(
                self.betas[order.destination - 1]
            )

            # NOTE: add new delivery from same area to self._next_deliveries
            self._next_deliveries.put((
                next_order_time,
                Delivery(
                    uuid.uuid4(),
                    next_order_time,
                    order.destination,
                )
            ))

        # NOTE: move processed deliveries to self.unassigned deliveries

        # NOTE: while (next WIP delivery is done processing)
        for order_time, order in self.processing_deliveries.iter_until(self.time):
            # NOTE: remove delivery from self.processing_deliveries

            # NOTE: add delivery to self.unassigned_deliveries
            # NOTE: indexed without branch
            self.unassigned_deliveries[order.destination - 1].append(order)
                
            # NOTE: update self.n_processing
            self.n_processing[order.destination - 1] -= 1

            # NOTE: update self.n_unassigned
            self.n_unassigned[order.destination - 1] += 1

            # NOTE: no need to update self.earliest_unassigned here because it is done further down

        # NOTE: execute any assignments
        assignment_matrix = action[:-self.n_riders]
        
        # NOTE: randomize order of areas so that no area receives more priority
        # NOTE: use index matching dist_mat
        area_idx_shuffle = [i + 1 for i in range(self.n_areas)]
            
        # NOTE: shuffle is in-place only
        np.random.shuffle(area_idx_shuffle)
            
        # NOTE: keep track of made assignments for extra info
        made_assignments = [[0 for j in range(self.n_riders)] for i in range(self.n_areas)]

        for area_idx in area_idx_shuffle:
            # NOTE: area_idx is indexed with the branch
            area_assignments = assignment_matrix[(area_idx - 1)*self.n_areas:area_idx*self.n_areas]
                
            # NOTE: check if assignment cannot be made due to rider being out
            for rider_idx, n_assign in enumerate(area_assignments):
                if n_assign <= 0:
                    continue
                if not self.rider_in_branch[rider_idx]:
                    continue

                # NOTE: the number to be assigned is min(remaining_capacity, n_in_unassigned, n_decided)
                remaining_capacity = self.rider_capacity - self.riders[rider_idx].n_deliveries
                n_in_unassigned = self.n_unassigned[area_idx - 1]

                n_assign_actual = min(remaining_capacity, n_in_unassigned, n_assign)
                if n_assign_actual <= 0:
                    continue

                # NOTE: add deliveries to the rider
                for delivery in self.unassigned_deliveries[area_idx - 1][:n_assign_actual]:
                    self.riders[rider_idx].assign_delivery(copy(delivery))

                # NOTE: remove deliveries from unassigned_deliveries
                self.unassigned_deliveries[area_idx - 1] = self.unassigned_deliveries[area_idx - 1][n_assign_actual:]

                # NOTE: update self.n_unassigned
                self.n_unassigned[area_idx - 1] -= n_assign_actual

                # NOTE: update self.n_assigned
                self.n_assigned[area_idx - 1][rider_idx] += n_assign_actual

                # NOTE: update made_assignments
                made_assignments[area_idx - 1][rider_idx] += n_assign_actual

            # NOTE: update self.earliest_unassigned
            if len(self.unassigned_deliveries[area_idx - 1]) > 0:
                self.earliest_unassigned[area_idx - 1] = min([deliv.order_time + self.delivery_lead_time for deliv in self.unassigned_deliveries[area_idx - 1]])
            else:
                self.earliest_unassigned[area_idx - 1] = self.episode_length*2

        # NOTE: execute any dispatchments
        dispatch_vector = action[-self.n_riders:]

        # NOTE: track made_dispatchments for extra info
        made_dispatchments = [0 for i in range(self.n_riders)]
        dispatchment_deliveries = [0 for i in range(self.n_riders)]

        for rider_idx, dispatch_decision in enumerate(dispatch_vector):
            if not dispatch_decision: continue

            # NOTE: check if dispatch cannot be made due to rider being out or having no assigned deliveries
            if not self.rider_in_branch[rider_idx]: continue
            if self.riders[rider_idx].n_deliveries <= 0: continue

            # NOTE: determine rider path using heuristic
            rider_destinations = [aidx for aidx, dels in self.riders[rider_idx].deliveries.items() if len(dels) > 0]
                # NOTE: just use a greedy pathing
                # NOTE: rider.deliveries is indexed with branch

            due_times = [min([d.order_time + self.delivery_lead_time for d in self.riders[rider_idx].deliveries[dest_]]) for dest_ in rider_destinations]
                

            # NOTE: update rider.path
            # NOTE: adding 0 so that the rider returns to branch after
            # self.riders[rider_idx].path = tsp_greedy(rider_destinations, self.dist_mat) + [0]
            self.riders[rider_idx].path = tsp_by_time(
                rider_destinations,
                due_times,
                self.dist_mat,
                self.time,
            ) + [0]

            # NOTE: update rider route
            self.riders[rider_idx].route = (0,self.riders[rider_idx].path[0])

            # NOTE: update self.rider_in_branch
            self.rider_in_branch[rider_idx] = 0

            # NOTE: update self.rider_destinations
            self.rider_destinations[rider_idx] = self.riders[rider_idx].path[0]
                
            # NOTE: update self.last_dispatch_time
            self.last_dispatch_time[rider_idx] = self.time
            
            # NOTE: compute travel time from distribution
            if not self.deterministic_travel_time:
                arrival_time = self.time + np.random.exponential(
                    self.dist_mat[0, self.riders[rider_idx].path[0]]
                )
            else:
                arrival_time = self.time + self.dist_mat[0, self.riders[rider_idx].path[0]]

            # NOTE: add arrival event to self._next_arrivals
            self._next_arrivals.put((
                arrival_time,
                Arrival(
                    rider_idx,
                    self.riders[rider_idx].path[0],
                    copy(self.time),
                )
            ))

            # NOTE: update made_dispatchments
            made_dispatchments[rider_idx] += 1

            # NOTE: update dispatchment_deliveries
            dispatchment_deliveries[rider_idx] = self.riders[rider_idx].n_deliveries

            # NOTE: log dispatch
            self.logger.log(
                time = self.time,
                event_type = "dispatch",
                rider = rider_idx,
                n_carried_before = self.riders[rider_idx].n_deliveries,
                n_carried_after = self.riders[rider_idx].n_deliveries,
                n_delivered = None,
                origin = 0,
                destination = self.rider_destinations[rider_idx],
                lateness = None,
                path = self.riders[rider_idx].path,
            )

        # NOTE: update self.earliest_undispached
        # self.earliest_undispatched = [self.episode_length*2 for i in range(self.n_areas)]
        self.earliest_undispatched = [[self.episode_length*2 for j in range(self.n_riders)] for i in range(self.n_areas)]
        for rider_idx in range(self.n_riders):
            if self.rider_in_branch[rider_idx]:
                for area_idx, deliveries in self.riders[rider_idx].deliveries.items():
                    for delivery in deliveries:
                        if delivery.order_time + self.delivery_lead_time < self.earliest_undispatched[delivery.destination - 1][rider_idx]:
                            self.earliest_undispatched[delivery.destination - 1][rider_idx] = delivery.order_time + self.delivery_lead_time

        # NOTE: track the number of late deliveries
        # NOTE: indexed without branch
        self.n_late_processing = [0 for i in range(self.n_areas)]
        self.n_late_unassigned = [0 for i in range(self.n_areas)]
        self.n_late_assigned = [0 for i in range(self.n_areas)]

        deliveries_in_system = [0 for i in range(self.n_areas)]

        # NOTE: add penalty for late deliveries
        # NOTE: check for late deliveries in processing deliveries
        for fin_time, delivery in self.processing_deliveries.queue:
            deliveries_in_system[delivery.destination - 1] += 1
            if delivery.order_time + self.delivery_lead_time < self.time:
                lateness_penalty += self.late_penalty_dt
                self.n_late_processing[delivery.destination - 1] += 1

        # NOTE: check for late deliveries in unassinged deliveries
        for del_set in self.unassigned_deliveries:
            for delivery in del_set:
                deliveries_in_system[delivery.destination - 1] += 1
                if delivery.order_time + self.delivery_lead_time < self.time:
                    lateness_penalty += self.late_penalty_dt
                    self.n_late_unassigned[delivery.destination - 1] += 1

        # NOTE: check for late deliveries in assigned deliveries (riders)
        for rider in self.riders:
            for area_idx, del_set in rider.deliveries.items():
                for delivery in del_set:
                    deliveries_in_system[delivery.destination - 1] += 1
                    if delivery.order_time + self.delivery_lead_time < self.time:
                        lateness_penalty += self.late_penalty_dt
                        self.n_late_assigned[delivery.destination - 1] += 1
        
        # NOTE: update self.time
        self.time += 1

        reward = early_reward - lateness_penalty - travel_penalty

        extra_info = {
            "early_reward": early_reward,
            "lateness_penalty": lateness_penalty,
            "travel_penalty": travel_penalty,
            "made_assignments": made_assignments,
            "made_dispatchments": made_dispatchments,
            "dispatchment_deliveries": dispatchment_deliveries,
            "ontime_deliveries": ontime_deliveries,
            "late_deliveries": late_deliveries,
            "deliveries_in_system": deliveries_in_system,
        }

        if self.time >= self.episode_length:
            self.is_done = True

        return self.get_state(), reward, self.is_done, extra_info

    def wait(self, n):
        do_nothing = [0 for i in range(self.n_areas*self.n_riders)] + [0 for j in range(self.n_riders)]
        cumulative_reward = 0
        for i in range(n):
            state, reward, is_done, _ = self.step(do_nothing)
            cumulative_reward += reward
        return state, reward, is_done, cumulative_reward

    def get_state(self):
        # NOTE: whenever this is updated, also update utils.explain_state()

        # NOTE: do I need to add information about the riders en route?
        # NOTE: can also add self.earliest_processing?
        # NOTE: do I need to add information about the number of currently late deliveries?

        state = []

        # NOTE: number of deliveries which are processing at the branch for each area
        state += copy(self.n_processing)

        # NOTE: number of deliveries which have been processed but are not yet assigned to riders
        # NOTE: for each area
        state += copy(self.n_unassigned)

        # NOTE: whether or not each rider is at the branch
        state += copy(self.rider_in_branch)
        
        # NOTE: the number of deliveies to each area assigned to each rider
        for assignments_ in self.n_assigned:
            state += copy(assignments_)

        # NOTE: the current destination of each rider
        state += copy(self.rider_destinations)

        # NOTE: the due time of the earliest delivery in unassigned deliveries per area
        state += copy(self.earliest_unassigned)

        # NOTE: the due time of the earliest delivery which has been assigned to a rider but not dispatched
        # NOTE: for each area and each rider
        # state += copy(self.earliest_undispatched)
        for times_ in self.earliest_undispatched:
            state += copy(times_)

        # NOTE: add time when the rider was last dispatched
        state += copy(self.last_dispatch_time)

        # NOTE: add the number of late deliveries
        state += copy(self.n_late_processing)
        state += copy(self.n_late_unassigned)
        state += copy(self.n_late_assigned)

        # NOTE: the current time
        state.append(copy(self.time))

        return state

def test_environment():
    import time

    from utils import explain_state

    np.random.seed(1)

    dist_mat = np.matrix([
        [0, 10, 15, 20],
        [12, 0, 8, 8],
        [15, 7, 0, 12],
        [20, 8, 14, 0],
    ])
    env = DispatcherEnv(
        dist_mat, 
        3,
        betas = [10, 8, 12],
    )

    current_state = env.get_state()

    # do nothing for 20 steps
    do_nothing = [0,0,0,0,0,0,0,0,0,0,0,0]
    for i in range(20):
        env.step(do_nothing)
    
    explain_state(env)

    # assign some deliveries to rider 2
    env.step([0,0,1,0,0,0,0,0,2,0,0,0])
    explain_state(env)

    # dispatch rider 2
    start_ = time.time()
    env.step([0,0,0,0,0,0,0,0,0,0,0,1])
    end_ = time.time()
    print("Step time:", end_ - start_)

    explain_state(env)

    # print(env.riders[2].route)
    # print(env.riders[2].path)
    # print(env._next_arrivals.peek())

    # at this point, the rider 2 should arrive at area 1
    
    # print(env.riders[2].deliveries)
    
    start_ = time.time()
    state, reward, is_done, cumulative_reward = env.wait(5)
    end_ = time.time()
    print("5 steps time:", end_ - start_)

    explain_state(env)
    print("cumulative reward:", cumulative_reward)

    print(env.riders[2].route)
    print(env.riders[2].path)
    print(env._next_arrivals.peek())

def time_environment():
    import time

    dist_mat = np.matrix([
        [0, 10, 15, 20],
        [12, 0, 8, 8],
        [15, 7, 0, 12],
        [20, 8, 14, 0],
    ])
    env = DispatcherEnv(
        dist_mat, 
        3,
        betas = [10, 8, 12],
    )

    current_state = env.get_state()

    start_ = time.time()
    do_nothing = [0,0,0,0,0,0,0,0,0,0,0,0]
    for i in range(1000):
        env.step(do_nothing)
    end_ = time.time()
    print("1000 steps time:", end_ - start_)

def test_priority_queue():
    my_pq = MyPriorityQueue()

    my_pq.put((5, "a"))
    my_pq.put((3, "b"))
    my_pq.put((7, "l"))

    print(my_pq)
    print(my_pq.qsize())
    print(my_pq.get())
    print(my_pq.peek())
    print(my_pq.qsize())

def test_tsp():
    # dist_mat = np.matrix([
    #     [0, 10, 15, 20],
    #     [12, 0, 8, 8],
    #     [15, 7, 0, 12],
    #     [20, 8, 14, 0],
    # ])

    dist_mat = np.matrix([
        [0, 10, 8, 20],
        [12, 0, 8, 8],
        [9, 7, 0, 12],
        [20, 8, 14, 0],
    ])

    print(tsp_greedy([1,3],dist_mat))

def test_preprocessing():
    from utils import preprocess_action

    np.random.seed(1)

    dist_mat = np.matrix([
        [0, 10, 15, 20],
        [12, 0, 8, 8],
        [15, 7, 0, 12],
        [20, 8, 14, 0],
    ])
    env = DispatcherEnv(
        dist_mat, 
        3,
        betas = [10, 8, 12],
    )

    print(
        preprocess_action(
            [0,0.28,0.14,0.56,0.96,0,0,0,0,0.33,0.5,0.87],
            env
        )
    )

if __name__ == "__main__":
    test_environment()
    # time_environment()
    # test_preprocessing()